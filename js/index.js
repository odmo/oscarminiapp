$(document).ready(function () {
    //console.log('funcionando');
    $('#submit').click(function (e) { 
      e.preventDefault();
      
      let uri = 'http://localhost:8888/oscarMiniApp/v1/controller/sessions.php';

      let username = $("#username").val();
      let validName = (username.length == 0 || /^\s+$/.test(username)) ? false : true ; 
      
      let password = $("#password").val();
      let validPass = (password.length == 0 ) ? false : true;
      
      if (validName && validPass){
        let jsonData = JSON.stringify({ "username": username, "password": password } );
        $.ajax({
          type: "POST",
          url: uri,
          contentType: 'application/json',
          dataType: "json",
          data: jsonData,
          success: function(json) {
            if (json.statusCode == 201 && json.success == true){
              var data = json.data;
              let access_token = data.access_token;
              let refresh_token = data.refresh_token;
              let session_id = data.session_id;
              sessionStorage.setItem('access_token', access_token );
              sessionStorage.setItem('refresh_token', refresh_token );
              sessionStorage.setItem('session_id', session_id );
              location.href='http://localhost:8888/oscarMiniApp/views/users.html';
            }
          }
        });
      }else {
          let invalidUsername = !validName ? " Invalid Username " : " " ;  
          let invalidPassword = !validPass ? " Invalid Password " : " " ;
          alert("something was wrong:" + invalidUsername + invalidPassword );
      }
    });
    

});
