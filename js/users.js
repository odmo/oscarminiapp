
var initPage = false;

$(document).ready(function () {
    //console.log('funcionando');

    if(initPage)return;
    initPage = true;

    let access_token = sessionStorage.getItem('access_token');
    let refresh_token = sessionStorage.getItem('refresh_token');
    let session_id = sessionStorage.getItem('session_id');
    
    let jsonData = JSON.stringify({ "refresh_token": refresh_token } );
    let sessionUri = 'http://localhost:8888/oscarMiniApp/v1/controller/sessions.php?sessionid='+session_id;
    $.ajax({
        type: 'PATCH',
        url: sessionUri,
        contentType: 'application/json',
        dataType: "json",
        data: jsonData,
        headers: {
            "Authorization": access_token
        },
        success: function(json) {
            console.log(json);
            var data = json.data;
            let access_token = data.access_token;
            let refresh_token = data.refresh_token;
            let session_id = data.session_id;
            sessionStorage.setItem('access_token', access_token );
            sessionStorage.setItem('refresh_token', refresh_token );
            sessionStorage.setItem('session_id', session_id );
        },
        error: function(json) {
            location.href='http://localhost:8888/oscarMiniApp/views/index.html';
        }
     });

    console.log(access_token);
    console.log(refresh_token);
    console.log(session_id);
    
    let page = 1;
    let uri = "http://localhost:8888/oscarMiniApp/v1/controller/users.php?page="+page;

    $.ajax({
        type: "GET",
        url: uri,
        contentType: 'application/json',
        dataType: "json",
        success: function(json) {
            if (json.statusCode == 200 && json.success == true){
                
                data = json.data.users;
                console.log(data);

                let nextPage = page + 1; 
                let prevPage = page - 1;
                
                let hasNextPage = json.data.has_next_page;
                console.log(hasNextPage);
                let hasPreviusPage = json.data.has_previus_page;
                console.log(hasPreviusPage);
                let totalPages = json.data.total_pages;
                console.log(totalPages);

                // fill userTable onLoad
                let fillTable = document.querySelector('#usersTable'); 
                fillTable.innerHTML = '';
                for (let item of data){
                    fillTable.innerHTML += `
                        <tr>
                            <td> 
                                <img src="${ ( item.useractive == 'Y') ? '../resources/check.png' : '../resources/fail.svg' }" class="icon" > 
                            </td>
                            <td>
                                <div> ${item.username} </div> 
                                <div>${item.fullname} </div> 
                            </td>
                            <td>
                                <div> ... </div> 
                                <div> ${item.group} </div>
                            </td>
                        </tr>
                    `
                }
                //fill paguination onload
                console.log('paguimatop');
                let fillPags = document.querySelector('#paguination'); 
                fillPags.innerHTML = '';
                fillPags.innerHTML +=  hasPreviusPage ?  `
                <div class = "col s1 pagButton" id = "btPrev" >
                    <button type = "submit" onclick="changePage(${prevPage})" class='col s12 '> Prev </button>
                </div>` : ``;
                for (var i = 1; i <= totalPages ; i++) {
                    fillPags.innerHTML += `
                    <div class = "col s1 pagButton" id = "bt${i}" >
                        <button type = "submit" onclick="changePage(${i})" class='col s12 '> ${i} </button>
                    </div>`;
                }
                fillPags.innerHTML +=  hasNextPage ?  `
                <div class = "col s1 pagButton" id = "btNext" >
                    <button type = "submit" onclick="changePage(${nextPage})" class='col s12 '> next </button>
                </div>` : ``;
            
            }
        }
    });
    

});

function changePage(page){
    
    let access_token = sessionStorage.getItem('access_token');
    let refresh_token = sessionStorage.getItem('refresh_token');
    let session_id = sessionStorage.getItem('session_id');
    
    console.log(access_token);
    console.log(refresh_token);
    console.log(session_id);

    let uri = "http://localhost:8888/oscarMiniApp/v1/controller/users.php?page="+page;

    $.ajax({
        type: "GET",
        url: uri,
        contentType: 'application/json',
        dataType: "json",
        success: function(json) {
            if (json.statusCode == 200 && json.success == true){
                
                data = json.data.users;
                console.log(data);

                let nextPage = page + 1; 
                let prevPage = page - 1;
                
                let hasNextPage = json.data.has_next_page;
                console.log(hasNextPage);
                let hasPreviusPage = json.data.has_previus_page;
                console.log(hasPreviusPage);
                let totalPages = json.data.total_pages;
                console.log(totalPages);

                // fill userTable onLoad
                let fillTable = document.querySelector('#usersTable'); 
                fillTable.innerHTML = '';
                for (let item of data){
                    fillTable.innerHTML += `
                        <tr>
                            <td> 
                                <img src="${ ( item.useractive == 'Y') ? '../resources/check.png' : '../resources/fail.svg' }" class="icon" > 
                            </td>
                            <td>
                                <div> ${item.username} </div> 
                                <div>${item.fullname} </div> 
                            </td>
                            <td>
                                <div> ... </div> 
                                <div> ${item.group} </div>
                            </td>
                        </tr>
                    `
                }
                //fill paguination onload
                console.log('paguimatop');
                let fillPags = document.querySelector('#paguination'); 
                fillPags.innerHTML = '';
                fillPags.innerHTML +=  hasPreviusPage ?  `
                <div class = "col s1 pagButton" id = "btPrev" >
                    <button type = "submit" onclick="changePage(${prevPage})" class='col s12 '> Prev </button>
                </div>` : ``;
                for (var i = 1; i <= totalPages ; i++) {
                    fillPags.innerHTML += `
                    <div class = "col s1 pagButton" id = "bt${i}" >
                        <button type = "submit" onclick="changePage(${i})" class='col s12 '> ${i} </button>
                    </div>`;
                }
                fillPags.innerHTML +=  hasNextPage ?  `
                <div class = "col s1 pagButton" id = "btNext" >
                    <button type = "submit" onclick="changePage(${nextPage})" class='col s12 '> next </button>
                </div>` : ``;
            
            }
        }
    });
    
}

function logOut() {
    let access_token = sessionStorage.getItem('access_token');
    let session_id = sessionStorage.getItem('session_id');
    
    let sessionUri = 'http://localhost:8888/oscarMiniApp/v1/controller/sessions.php?sessionid='+session_id;
    $.ajax({
        type: 'DELETE',
        url: sessionUri,
        contentType: 'application/json',
        dataType: "json",
        headers: {
            "Authorization": access_token
        },
        success: function(json) {
            console.log(json);
            location.href='http://localhost:8888/oscarMiniApp/views/index.html';
        }
     });
}