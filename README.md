> oscarMiniApp

> video 

video demonstration : 
 `https://youtu.be/0UlqpGHHFUs`

> intructions to run this project


* install mamp
https://www.mamp.info/en/mamp/mac/

mamp install and configure automatically our local environment

* open mamp and launch the services, APACHE SERVER and MySql SERVER is availble now  

http://localhost:8888/MAMP/?language=English

* APACHE SERVER port : 8888   
* MySql SERVER port : 8889  default user name and password to adminitrate is "root". You can administrate this from phpMyAdmin pre-installed in mamp 
http://localhost:8888/phpMyAdmin/?lang=en

documentation
https://www.phpmyadmin.net/

* Create the bd estructure and tables from the next script. 
https://gist.github.com/odmox/6270c45da5aa2907392786d51583e46f

* Clone the proyect in your local folder : /Applications/MAMP/htdocs/

  `  $ git clone https://gitlab.com/odmo/oscarminiapp.git`


> FRONTEND

* login screen 
url :  http://localhost:8888/oscarMiniApp/views/

this screen, create a session record on the bd. the user account need to be created previous

screen image : https://images.guru/i/QEEaw

test : https://images.guru/i/QEUQ8


* users screen 

url : http://localhost:8888/oscarMiniApp/views/users.html

This screen shows a list of the users, the group they belong to and their status.

screem image : https://images.guru/i/QEBRs



> BACKEND

* APIs:

* create user 
Create a new user acount  

```
    uri : http://localhost:8888/oscarMiniApp/v1/controller/users.php
    request method : POST
    content type : application/json
    raw json example 
    {
        "fullname" : "test",
        "username" : "test",
        "password" : "test"
    }
```


```
    success result example 
    {
    "statusCode": 201,
    "success": true,
    "message": [
        " user created "
    ],
    "data": {
        "user_id": "5",
        "fullname": "test",
        "username": "test"
    }
    }
```

* getUsers 
return te users acounts from the db filtered and paginated, for more speed up querys 


```
    uri : http://localhost:8888/oscarMiniApp/v1/controller/users.php?page=1
    params : {
         "page" : 1
    }
    request method : GET
    content type : application/json
    
```


```
    {
    "statusCode": 200,
    "success": true,
    "message": [],
    "data": {
        "rows_returned": 5,
        "total_rows": 5,
        "total_pages": 1,
        "has_next_page": false,
        "has_previus_page": false,
        "users": [
            {
                "id": 1,
                "fullname": "oscar moreno",
                "username": "Y",
                "password": "$2y$10$RrlBo2VbG8/X8AzHyurtnuvNHNiki/3.tzZCpi0DKGcn0CI/58Tku",
                "useractive": "Y",
                "loginattempts": 0,
                "group": "Default  group"
            },
            {
                "id": 2,
                "fullname": "isaac moreno",
                "username": "Y",
                "password": "$2y$10$AWTe/uf28QxFAJA6Odnnk.ylWuoRSnTjcm2pGCOzEqrDW7bTnltOm",
                "useractive": "Y",
                "loginattempts": 0,
                "group": "Default  group"
            },
            {
                "id": 3,
                "fullname": "jonathan moreno",
                "username": "Y",
                "password": "$2y$10$sa4sUi/uD0zav.bAAGBFP.JecJ0b0yol2isb0oKRF.IGG2hWCUfke",
                "useractive": "Y",
                "loginattempts": 0,
                "group": "Default  group"
            },
            {
                "id": 4,
                "fullname": "bianca nevarez",
                "username": "Y",
                "password": "$2y$10$036RzO7QnZu9QQefS0VobOomnPp41LVfOZSz1F8.iVGcfwMA.1ARu",
                "useractive": "Y",
                "loginattempts": 0,
                "group": "Default  group"
            },
            {
                "id": 5,
                "fullname": "test",
                "username": "Y",
                "password": "$2y$10$0vX/yB2TGPXXkdNqNUCiHedUnTI7RKgYaW40GuZz7zDERdbUJ4gny",
                "useractive": "Y",
                "loginattempts": 0,
                "group": "Default  group"
            }
        ]
    }
    }
```

* login Session 
create a session and generate an access token and refreh token to update the session, if loggin failed attemps become three or more the account is locked, if login success the account attemps reset to cero.


```
    uri : http://localhost:8888/oscarMiniApp/v1/controller/sessions.php
    request method : POST
    content type : application/json
    raw json example 
    {
        "username" : "test",
        "password" : "test"
    }
```

```
    success result example 
    {
    "statusCode": 201,
    "success": true,
    "message": [],
    "data": {
        "session_id": 4,
        "access_token": "ZmZmZGE5OGMwY2U5YTgyM2MwNjc4ODY0YTM0MzY4NjhkZDMzZWQ1ZTVmY2U4ZWNhMTU5NTkyODUxMA==",
        "access_token_expires_in": 1200,
        "refresh_token": "NTc5ZTM3ZmQyYWJhYzg1YjhlNjE0NTk2YTU5YzE0NGMxMmNjODU5Y2ExZjY3MGVlMTU5NTkyODUxMA==",
        "refresh_token_expires_in": 1209600
    }
    }
```



* logout Session 
close or delete the active session associate to this device.  

```
    uri: http://localhost:8888/oscarMiniApp/v1/controller/sessions.php?sessionid=3
    Query Params : {
        "sessionid" : 3
    }
    HEADERS : {
        "Authorization" : "MThlYWJkM2U1ZTkyOGM3ZjM3MDEzNTgwMTM5ZWVhMTAyMmMyZTQ4N2M5YzlhNWQ3MTU5NTY0NzkyNw=="
    }
    request method : DELETE
    content type : application/json
```


```
    success result example 
    {
    "statusCode": 200,
    "success": true,
    "message": [
        " logget out "
    ],
    "data": {
        "session_id": 4
    }
    }
```


* refresh token
update the access token and refresh token to keep live the active session 



```
    uri : http://localhost:8888/oscarMiniApp/v1/controller/sessions.php?sessionid=5
    Query Params : {
        "sessionid" : 5
    }
    HEADERS : {
        "Authorization" : "Yjk3YjRiOTY4MmVhZDZiMDAxMmUzZjBhYzk0OGFmMzVmZDc4MzcxYzA5NWY5Y2NlMTU5NTkzMDE4Ng=="
    }
    request method : PATCH
    content type : application/json
    raw json example 
    {
        "refresh_token" : "NWE0Y2Y2ODVmNWJiMTllYmFmMGU4ODZkNjcwZTg2NzNiYzY3YmQxYjI1MmRlMWJiMTU5NTkzMDE4Ng=="
    }
```


```
    success result example 
    {
    "statusCode": 200,
    "success": true,
    "message": [
        " Token refreshed "
    ],
    "data": {
        "session_id": 5,
        "access_token": "OGY3NjQ2NzU2N2U4OWZhNmIxZWJiODQzZGYyM2Y1YmQ1YjdiNDVhMTM1YTEzMzJlMzEzNTM5MzUzOTMzMzAzNTMxMzg=",
        "access_token_expiry": 1200,
        "refresh_token": "YWNlOGZjNGIwOWRlNDg0YTIwZjRlYzNkOWQwODAyZGJkOGVjMjBiZjlkZTQxNTNjMzEzNTM5MzUzOTMzMzAzNTMxMzg=",
        "refresh_token_expiry": 1209600
    }
    }
```

