<?php

    class UserException extends Exception {}
    
    class User {
        private $_id;
        private $_fullname;
        private $_username;
        private $_password;
        private $_useractive;
        private $_loginattempts;
        private $_group;

        public function __construct($id, $fullname, $username, $password, $useractive, $loginattempts, $group)
        {
            $this->setID($id);
            $this->setFullname($fullname);
            $this->setUsername($username);
            $this->setPassword($password);
            $this->setUseractive($useractive);
            $this->setLoginattempts($loginattempts);
            $this->setGroup($group);
        }

        public function getID(){
            return $this->_id;
        }
        public function getFullname(){
            return $this->_fullname;
        }
        public function getUsername(){
            return $this->_username;
        }
        public function getPassword(){
            return $this->_password;
        }
        public function getUseractive(){
            return $this->_useractive;
        }
        public function getLoginattempts(){
            return $this->_loginattempts;
        }
        public function getGroup(){
            return $this->_group;
        }

        public function setID($id){

            if ( ( $id !== null ) && ( !is_numeric( $id ) || ( $id <= 0 ) || ( $id > 9223372036854775807 ) || ( $this->_id !== null ) ) ) {
                throw new UserException( " User ID error : ". $id );
            }
            $this->_id = $id;
        }
        public function setFullname($fullname){
            if ( (strlen($fullname) < 0) || (strlen($fullname) > 255) ) {
                throw new UserException( " Task fullname error : ". $fullname );
            }
            $this->_fullname = $fullname;
        }
        public function setUsername($username){
            if ( (strlen($username) < 0) || (strlen($username) > 255) ) {
                throw new UserException( " Task username error : ". $username );
            }
            $this->_username = $username;
        }
        public function setPassword($password){
            if ( (strlen($password) < 0) || (strlen($password) > 255) ) {
                throw new UserException( " Task password error : ". $password );
            }
            $this->_password = $password;
        }
        public function setUseractive($useractive){
            if ( ( strtoupper($useractive) !== 'Y' ) && ( strtoupper($useractive) !== 'N' ) ){
                throw new UserException( " Task useractive error : ". $useractive );
            }
            $this->_useractive = $useractive;
        }
        public function setLoginattempts($loginattempts){

            if ( ( $loginattempts !== null ) && ( !is_numeric( $loginattempts ) || ( $loginattempts < 0 ) || ( $loginattempts > 9223372036854775807 ) || ( $this->_idloginattempts !== null ) ) ) {
                throw new UserException( " User loginattempts error : ". $loginattempts );
            }
            $this->_loginattempts = $loginattempts;
        }
        public function setGroup ($group){
            if ( (strlen($group) < 0) || (strlen($group) > 255) ) {
                throw new UserException( " Task group error : ". $group );
            }
            $this->_group = $group;
        }

        public function returnUserAsArray(){
            $user = array();
            $user['id'] = $this->getID();
            $user['fullname'] = $this->getFullname();
            $user['username'] = $this->getUsername();
            $user['password'] = $this->getPassword();
            $user['useractive'] = $this->getUseractive();
            $user['loginattempts'] = $this->getLoginattempts();
            $user['group'] = $this->getGroup();
            return $user;
        }
    }